<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ftm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${empty cookie.lang ? pageContext.response.locale : cookie.lang.value}"/>
<fmt:setBundle basename="message"/>
<fmt:setBundle basename="languages" var="languages"/>
<html>
<head>
    <title><fmt:message key="todo.app.name"/></title>
    <link rel="stylesheet" href="webjars/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="webjars/font-awesome/5.11.2/css/fontawesome.css" rel="stylesheet">
    <link href="webjars/font-awesome/5.11.2/css/brands.css" rel="stylesheet">
    <link href="webjars/font-awesome/5.11.2/css/solid.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="d-flex flex-row-reverse">
        <a href="tasks?lang=en_GB"><fmt:message key="language.english" bundle="${languages}"/></a>&nbsp;|&nbsp;
        <a href="tasks?lang=de_DE"><fmt:message key="language.german" bundle="${languages}"/></a>&nbsp;|&nbsp;
        <a href="tasks?lang=pl_PL"><fmt:message key="language.polish" bundle="${languages}"/></a>
    </div>
    <div class="d-flex justify-content-center">
        <fmt:message key="todo.app.name" var="appTitle"/>
        <h1 class="display-2"><strong>${fn:split(appTitle, ' ')[0]}</strong> ${fn:split(appTitle, ' ')[1]}</h1>
    </div>
    <div class="d-flex justify-content-center">
        <form method="post" action="tasks">
            <div class="input-group form-group">
                <label for="description" class="sr-only"><fmt:message key="todo.task"/></label>
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-plus-square"></i></span>
                </div>
                <input type="text" class="form-control form-control-lg" id="description"
                       placeholder="<fmt:message key="todo.task.hint"/>" size="64"
                       name="description" autocomplete="off" required>
                <div class="input-group-append">
                    <input class="btn btn-outline-secondary" type="submit" id="button-addon2"
                           value="<fmt:message key="todo.save"/>">
                </div>
            </div>
            <div class="form-row align-items-center">
                <div class="form-group col-md-4">
                    <label for="finishDate" class="sr-only"><fmt:message key="todo.date.finish"/></label>
                    <input type="datetime-local" id="finishDate" name="finishDate" class="form-control form-control-sm">
                </div>
                <div class="form-group col-md-4">
                    <label for="priority" class="sr-only"><fmt:message key="todo.priority"/></label>
                    <select id="priority" name="priority" class="form-control form-control-sm">
                        <option value="HIGH"><fmt:message key="priority.high"/></option>
                        <option value="NORMAL" selected><fmt:message key="priority.normal"/></option>
                        <option value="LOW"><fmt:message key="priority.low"/></option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    <c:forEach var="task" items="${requestScope.taskList}">
        <div class="d-flex justify-content-center">
            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title">${task.description}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${task.finishDate}</h6>
                    <c:url var="deleteUrl" value="tasks">
                        <c:param name="action" value="delete"/>
                        <c:param name="id" value="${task.id}"/>
                    </c:url>
                    <c:choose>
                        <c:when test="${task.priority == 'HIGH'}">
                            <i class="fas fa-angle-double-up card-link text-danger"></i>
                        </c:when>
                        <c:when test="${task.priority == 'LOW'}">
                            <i class="fas fa-chevron-down card-link text-secondary"></i>
                        </c:when>
                    </c:choose>
                    <a href="${deleteUrl}" class="card-link text-muted"><i class="fas fa-trash-alt"></i></a>
                </div>
            </div>
        </div>
    </c:forEach>
    <c:if test="${empty requestScope.taskList}">
        <div class="d-flex justify-content-center"><ftm:message key="task.list.empty"/></div>
    </c:if>
</div>
<script src="webjars/jquery/3.0.0/jquery.min.js"></script>
<script src="webjars/popper.js/1.14.3/popper.min.js"></script>
<script src="webjars/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>